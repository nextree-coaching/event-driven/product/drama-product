/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.product.aggregate.product.domain.logic;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sample.event.product.aggregate.product.store.ProductStore;
import io.naraplatform.daysboy.EventStream;
import sample.event.product.aggregate.product.api.command.command.ProductCommand;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naradrama.prologue.domain.cqrs.FailureMessage;
import java.util.Optional;
import sample.event.product.aggregate.product.domain.entity.sdo.ProductCdo;
import sample.event.product.aggregate.product.domain.event.ProductEvent;
import io.naradrama.prologue.domain.NameValueList;
import java.util.List;
import java.util.stream.Collectors;
import sample.event.product.aggregate.product.domain.entity.Product;
import java.util.NoSuchElementException;
import io.naradrama.prologue.util.spacekeeper.filter.drama.DramaRequest;
import io.naradrama.prologue.util.spacekeeper.support.DramaRequestContext;

@Service
@Transactional
public class ProductLogic {
    //
    private final ProductStore productStore;
    private final EventStream eventStream;

    public ProductLogic(ProductStore productStore, EventStream eventStream) {
        /* Autogen by nara studio */
        this.productStore = productStore;
        this.eventStream = eventStream;
    }

    public ProductCommand routeCommand(ProductCommand command) {
        /* Autogen by nara studio */
        switch(command.getCqrsBaseCommandType()) {
            case Register:
                if (command.isMultiCdo()) {
                    List<String> entityIds = this.registerProducts(command.getProductCdos());
                    command.setCommandResponse(new CommandResponse(entityIds));
                } else {
                    String entityId = Optional.ofNullable(command.getNameValues()).filter(nameValueList -> command.getNameValues().size() != 0).map(nameValueList -> this.registerProduct(command.getProductCdo(), nameValueList)).orElse(this.registerProduct(command.getProductCdo()));
                    command.setCommandResponse(new CommandResponse(entityId));
                }
                break;
            case Modify:
                this.modifyProduct(command.getEntityId(), command.getNameValues());
                command.setCommandResponse(new CommandResponse(command.getEntityId()));
                break;
            case Remove:
                this.removeProduct(command.getEntityId());
                command.setCommandResponse(new CommandResponse(command.getEntityId()));
                break;
            default:
                command.setFailureMessage(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }

    public String registerProduct(ProductCdo productCdo) {
        /* Autogen by nara studio */
        Product product = new Product(productCdo);
        if (productStore.exists(product.getEntityId())) {
            throw new IllegalArgumentException("product already exists. " + product.getEntityId());
        }
        productStore.create(product);
        ProductEvent productEvent = ProductEvent.newProductRegisteredEvent(product);
        eventStream.publishEvent(productEvent);
        return product.getEntityId();
    }

    public String registerProduct(ProductCdo productCdo, NameValueList nameValueList) {
        /* Autogen by nara studio */
        Product product = Product.newInstance(productCdo, nameValueList);
        if (productStore.exists(product.getEntityId())) {
            throw new IllegalArgumentException("product already exists. " + product.getEntityId());
        }
        productStore.create(product);
        ProductEvent productEvent = ProductEvent.newProductRegisteredEvent(product);
        eventStream.publishEvent(productEvent);
        return product.getEntityId();
    }

    public List<String> registerProducts(List<ProductCdo> productCdos) {
        /* Autogen by nara studio */
        return productCdos.stream().map(productCdo -> this.registerProduct(productCdo)).collect(Collectors.toList());
    }

    public Product findProduct(String entityId) {
        /* Autogen by nara studio */
        Product product = productStore.retrieve(entityId);
        if (product == null) {
            throw new NoSuchElementException("Product id: " + entityId);
        }
        return product;
    }

    public void modifyProduct(String entityId, NameValueList nameValues) {
        /* Autogen by nara studio */
        Product product = findProduct(entityId);
        DramaRequest dramaRequest = DramaRequestContext.current();
        product.setModifyId(dramaRequest.getLoginId());
        product.modify(nameValues);
        productStore.update(product);
        ProductEvent productEvent = ProductEvent.newProductModifiedEvent(entityId, nameValues, product);
        eventStream.publishEvent(productEvent);
    }

    public void removeProduct(String entityId) {
        /* Autogen by nara studio */
        Product product = findProduct(entityId);
        productStore.delete(product);
        ProductEvent productEvent = ProductEvent.newProductRemovedEvent(product.getEntityId(), product);
        eventStream.publishEvent(productEvent);
    }

    public boolean existsProduct(String entityId) {
        /* Autogen by nara studio */
        return productStore.exists(entityId);
    }

    public void handleEventForProjection(ProductEvent productEvent) {
        /* Autogen by nara studio */
        switch(productEvent.getCqrsDataEventType()) {
            case Registered:
                productStore.create(productEvent.getProduct());
                break;
            case Modified:
                Product product = productStore.retrieve(productEvent.getEntityId());
                product.modify(productEvent.getNameValues());
                productStore.update(product);
                break;
            case Removed:
                productStore.delete(productEvent.getEntityId());
                break;
        }
    }
}
