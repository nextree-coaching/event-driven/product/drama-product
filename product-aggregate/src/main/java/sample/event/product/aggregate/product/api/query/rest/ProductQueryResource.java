/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.product.aggregate.product.api.query.rest;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import lombok.RequiredArgsConstructor;
import sample.event.product.aggregate.product.api.query.rest.ProductQueryFacade;
import sample.event.product.aggregate.product.store.ProductStore;
import javax.persistence.EntityManager;
import io.naradrama.prologue.domain.cqrs.query.QueryResponse;
import sample.event.product.aggregate.product.api.query.query.ProductQuery;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import sample.event.product.aggregate.product.api.query.query.ProductDynamicQuery;
import sample.event.product.aggregate.product.api.query.query.ProductsDynamicQuery;

@RestController
@RequestMapping("/aggregate/product/product/query")
@RequiredArgsConstructor
public class ProductQueryResource implements ProductQueryFacade {
    //
    private final ProductStore productStore;
    private final EntityManager entityManager;

    @Override
    @PostMapping("/")
    public QueryResponse execute(@RequestBody ProductQuery productQuery) {
        /* Autogen by nara studio */
        productQuery.execute(productStore);
        return productQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-single")
    public QueryResponse execute(@RequestBody ProductDynamicQuery productDynamicQuery) {
        /* Autogen by nara studio */
        productDynamicQuery.execute(new RdbQueryRequest<>(entityManager));
        return productDynamicQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-multi")
    public QueryResponse execute(@RequestBody ProductsDynamicQuery productsDynamicQuery) {
        /* Autogen by nara studio */
        productsDynamicQuery.execute(new RdbQueryRequest<>(entityManager));
        return productsDynamicQuery.getQueryResponse();
    }
}
