/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.product.aggregate.product.api.command.rest;

import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import sample.event.product.aggregate.product.api.command.command.ProductCommand;
import sample.event.product.aggregate.product.domain.logic.ProductLogic;
import sample.event.product.aggregate.product.api.command.rest.ProductFacade;

@Slf4j
@RestController
@RequestMapping("/aggregate/product")
public class ProductResource implements ProductFacade {
    /* Autogen by nara studio */
    private final ProductLogic productLogic;

    public ProductResource(ProductLogic productLogic) {
        /* Autogen by nara studio */
        this.productLogic = productLogic;
    }

    @Override
    @PostMapping("/product/command")
    public CommandResponse executeProduct(@RequestBody ProductCommand productCommand) {
        /* Autogen by nara studio */
        return productLogic.routeCommand(productCommand).getCommandResponse();
    }

    @GetMapping("/hello")
    public String hello() {
        return /* Autogen by nara studio */
        "Hello World!";
    }
}
