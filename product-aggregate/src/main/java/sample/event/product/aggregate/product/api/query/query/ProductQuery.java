/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.product.aggregate.product.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsBaseQuery;
import sample.event.product.aggregate.product.domain.entity.Product;
import sample.event.product.aggregate.product.store.ProductStore;

@Getter
@Setter
@NoArgsConstructor
public class ProductQuery extends CqrsBaseQuery<Product> {
    /* Autogen by nara studio */
    private String entityId;

    public void execute(ProductStore productStore) {
        /* Autogen by nara studio */
        setQueryResult(productStore.retrieve(entityId));
    }
}
