/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.product.aggregate.product.domain.event;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEvent;
import sample.event.product.aggregate.product.domain.entity.Product;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEventType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class ProductEvent extends CqrsDataEvent {
    /* Autogen by nara studio */
    private Product product;
    private String entityId;
    private NameValueList nameValues;

    protected ProductEvent(CqrsDataEventType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static ProductEvent newProductRegisteredEvent(Product product) {
        /* Autogen by nara studio */
        ProductEvent event = new ProductEvent(CqrsDataEventType.Registered);
        event.setProduct(product);
        return event;
    }

    public static ProductEvent newProductModifiedEvent(String entityId, NameValueList nameValues, Product product) {
        /* Autogen by nara studio */
        ProductEvent event = new ProductEvent(CqrsDataEventType.Modified);
        event.setEntityId(entityId);
        event.setNameValues(nameValues);
        event.setProduct(product);
        return event;
    }

    public static ProductEvent newProductRemovedEvent(String entityId, Product product) {
        /* Autogen by nara studio */
        ProductEvent event = new ProductEvent(CqrsDataEventType.Removed);
        event.setEntityId(entityId);
        event.setProduct(product);
        return event;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static ProductEvent fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, ProductEvent.class);
    }
}
