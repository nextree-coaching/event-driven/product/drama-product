/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.product.aggregate.product.store.maria.repository;

import javax.persistence.QueryHint;
import org.springframework.data.jpa.repository.JpaRepository;
import sample.event.product.aggregate.product.store.maria.jpo.ProductJpo;

public interface ProductMariaRepository extends JpaRepository<ProductJpo, String> {
    /* Autogen by nara studio */
}
