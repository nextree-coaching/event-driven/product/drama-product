/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.product.aggregate.product.api.query.rest;

import io.naradrama.prologue.domain.cqrs.query.QueryResponse;
import sample.event.product.aggregate.product.api.query.query.ProductQuery;
import sample.event.product.aggregate.product.api.query.query.ProductDynamicQuery;
import sample.event.product.aggregate.product.api.query.query.ProductsDynamicQuery;

public interface ProductQueryFacade {
    /* Autogen by nara studio */
    QueryResponse execute(ProductQuery productQuery);
    QueryResponse execute(ProductDynamicQuery productDynamicQuery);
    QueryResponse execute(ProductsDynamicQuery productsDynamicQuery);
}
