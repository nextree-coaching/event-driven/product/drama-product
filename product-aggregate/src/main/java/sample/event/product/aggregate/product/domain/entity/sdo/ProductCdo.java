package sample.event.product.aggregate.product.domain.entity.sdo;

import io.naradrama.prologue.domain.stage.ActorKey;
import io.naradrama.prologue.util.json.JsonSerializable;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import sample.event.product.aggregate.product.domain.entity.vo.Category;
import sample.event.product.aggregate.product.domain.entity.vo.Spec;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductCdo implements JsonSerializable {
    //
    private ActorKey actorKey;

    private String name;
    private Category category;
    private String description;
    private byte[] image;
    private Spec spec;

    public String toString() {
        //
        return toJson();
    }

    public static ProductCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ProductCdo.class);
    }

    public static ProductCdo sample() {
        //
        String productName = "dokdo belongs to korea";
        String productDescription = "텅 부분에 브랜드명을 새겨 넣어 브랜드 고유의 아이덴티티를 드러낸 모델로 소가죽, 고탄성 스판 소재로 출시되었습니다.";

        return new ProductCdo(ActorKey.sample(),
                productName,
                Category.Sneakers,
                productDescription,
                null,
                Spec.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
