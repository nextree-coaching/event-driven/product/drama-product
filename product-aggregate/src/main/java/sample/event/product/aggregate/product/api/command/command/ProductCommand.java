/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.product.aggregate.product.api.command.command;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommand;
import sample.event.product.aggregate.product.domain.entity.sdo.ProductCdo;
import java.util.List;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommandType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class ProductCommand extends CqrsBaseCommand {
    //
    private ProductCdo productCdo;
    private List<ProductCdo> productCdos;
    private boolean multiCdo;
    private String entityId;
    private NameValueList nameValues;

    protected ProductCommand(CqrsBaseCommandType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static ProductCommand newRegisterProductCommand(ProductCdo productCdo) {
        /* Autogen by nara studio */
        ProductCommand command = new ProductCommand(CqrsBaseCommandType.Register);
        command.setProductCdo(productCdo);
        return command;
    }

    public static ProductCommand newRegisterProductCommand(List<ProductCdo> productCdos) {
        /* Autogen by nara studio */
        ProductCommand command = new ProductCommand(CqrsBaseCommandType.Register);
        command.setProductCdos(productCdos);
        command.setMultiCdo(true);
        return command;
    }

    public static ProductCommand newModifyProductCommand(String entityId, NameValueList nameValues) {
        /* Autogen by nara studio */
        ProductCommand command = new ProductCommand(CqrsBaseCommandType.Modify);
        command.setEntityId(entityId);
        command.setNameValues(nameValues);
        return command;
    }

    public static ProductCommand newRemoveProductCommand(String entityId) {
        /* Autogen by nara studio */
        ProductCommand command = new ProductCommand(CqrsBaseCommandType.Remove);
        command.setEntityId(entityId);
        return command;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static ProductCommand fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, ProductCommand.class);
    }

    public static ProductCommand sampleForRegister() {
        /* Autogen by nara studio */
        return newRegisterProductCommand(ProductCdo.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sampleForRegister());
    }
}
