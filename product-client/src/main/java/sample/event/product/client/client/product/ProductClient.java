/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.product.client.client.product;

import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import sample.event.product.aggregate.product.api.command.command.ProductCommand;

public interface ProductClient {
    /* Autogen by nara studio */
    @PostMapping
    public CommandResponse executeProduct(@RequestBody ProductCommand productCommand);
    @GetMapping
    public String hello();
}
