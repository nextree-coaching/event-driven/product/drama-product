package sample.event.product.flow.product.logic;

import sample.event.product.aggregate.product.domain.entity.Product;
import sample.event.product.aggregate.product.domain.event.ProductEvent;
import sample.event.product.event.product.ProductDomainEvent;
import io.naradrama.prologue.domain.stage.ActorKey;
import io.naraplatform.daysboy.EventStream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class ProductHandlerLogic {
    //
    private final EventStream eventStreamService;


    public void onProduct(ProductEvent event) {
        //
        ProductDomainEvent domainEvent = null;
        switch (event.getCqrsDataEventType()) {
            case Registered:
                domainEvent = newProduct(event);
                break;
            case Modified:
                domainEvent = ProductDomainEvent.genModifiedEvent(event.getProduct().getEntityId(), event.getNameValues());
                break;
            case Removed:
                domainEvent = ProductDomainEvent.genRemovedEvent(event.getProduct().getEntityId());
                break;
        }

        if(domainEvent != null) {
            eventStreamService.publishEvent(domainEvent);
            log.debug("ProductDomainEvent published========\n{}", domainEvent);
        }
    }

    private ProductDomainEvent newProduct(ProductEvent event) {
        //
        Product product = event.getProduct();

        return ProductDomainEvent.genRegisteredEvent(product.getEntityId(),
                product.getName(),
                product.getDescription(),
                product.getCategory().toString(),
                product.getSpec().getColor().toHex(),
                product.getSpec().getSize(),
                product.getSpec().getMaterial().toString(),
                ActorKey.fromId(product.getActorId()));
    }
}
