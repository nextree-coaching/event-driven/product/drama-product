package sample.event.product.facade.event.listener.data;

import sample.event.product.aggregate.product.domain.event.ProductEvent;
import sample.event.product.flow.product.logic.ProductHandlerLogic;
import io.naraplatform.daysboy.event.DaysmanEventHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;


@Slf4j
@Component
@RequiredArgsConstructor
@Transactional
public class ProductDataEventHandler {
    //
    private final ProductHandlerLogic productHandlerLogic;

    @DaysmanEventHandler
    public void handle(ProductEvent event) {
        //
        productHandlerLogic.onProduct(event);
    }
}
